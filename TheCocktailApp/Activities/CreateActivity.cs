﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TheCocktailApp.Database;
using TheCocktailApp.Database.Tables;

namespace TheCocktailApp.Activities
{
    [Activity(Label = "CreateActivity", Theme = "@style/AppTheme")]
    public class CreateActivity : Activity
    {
        DatabaseHandler dbh;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.create_cocktail);
            // Create your application here
            dbh = new DatabaseHandler();
            Button btnSaveCocktail = FindViewById<Button>(Resource.Id.btnSaveCocktail);
            Button btnAddIngredient = FindViewById<Button>(Resource.Id.btnAddIngredient);
            btnAddIngredient.Click += BtnAddIngredient_Click;
            btnSaveCocktail.Click += BtnSaveCocktail_Click;

            SetUpDropdownLists();
        }

        private void BtnSaveCocktail_Click(object sender, EventArgs e)
        {
            EditText inpName = FindViewById<EditText>(Resource.Id.inpCocktailName);
            EditText inpDescription = FindViewById<EditText>(Resource.Id.inpCocktailDescription);
            EditText inpAlcoholPercentage = FindViewById<EditText>(Resource.Id.inpAlcoholPercentage);
            EditText inpProcedure = FindViewById<EditText>(Resource.Id.inpCocktailProcedure);

            Cocktail cocktail = new Cocktail();
            cocktail.Name = inpName.Text;
            cocktail.Description = inpDescription.Text;
            cocktail.AlcoholPercentage = int.Parse(inpAlcoholPercentage.Text);
            cocktail.Procedure = inpProcedure.Text;

            dbh.InsertCocktail(cocktail);

            // TODO = Ingredients


            Finish();
        }

        private void BtnAddIngredient_Click(object sender, EventArgs e)
        {
            Intent createIngredientActivity = new Intent(this, typeof(CreateIngredientActivity));
            StartActivity(createIngredientActivity);
            this.Finish();
        }

        private void SetUpDropdownLists()
        {
            SetUpIngredientSpinner();
            SetUpGlassSpinner();
            SetUpUnitSpinner();
        }

        private void SetUpIngredientSpinner()
        {
            Spinner ingredientSpinner = FindViewById<Spinner>(Resource.Id.ingredientSpinner);

            List<Ingredient> ingredients = dbh.GetAllIngredients();
            ArrayAdapter ingredientAdapter = new ArrayAdapter<Ingredient>(this, Android.Resource.Layout.SimpleSpinnerItem, ingredients);
            ingredientSpinner.Adapter = ingredientAdapter;
        }

        private void SetUpGlassSpinner()
        {
            Spinner glassSpinner = FindViewById<Spinner>(Resource.Id.glassSpinner);

            List<Glass> glasses = dbh.GetAllGlasses();
            ArrayAdapter glassAdapter = new ArrayAdapter<Glass>(this, Android.Resource.Layout.SimpleSpinnerItem, glasses);
            glassSpinner.Adapter = glassAdapter;
        }

        private void SetUpUnitSpinner()
        {
            Spinner unitSpinner = FindViewById<Spinner>(Resource.Id.unitSpinner);

            List<Unit> units = dbh.GetAllUnits();
            ArrayAdapter unitAdapter = new ArrayAdapter<Unit>(this, Android.Resource.Layout.SimpleSpinnerItem, units);
            unitSpinner.Adapter = unitAdapter;
        }

        protected override void OnResume()
        {
            base.OnResume();

            SetUpIngredientSpinner();
        }
    }
}