﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TheCocktailApp.Database.Tables;
using TheCocktailApp.Database;

namespace TheCocktailApp.Activities
{
    [Activity(Label = "DetailActivity", Theme = "@style/AppTheme")]
    public class DetailActivity : Activity
    {
        DatabaseHandler dbh;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.cocktail_detail);

            // Create your application here
            dbh = new DatabaseHandler();
            FillInDetails(Intent.GetIntExtra("CocktailID", 0));
        }

        private void FillInDetails(int cocktailId)
        {
            ImageView cocktailImage = FindViewById<ImageView>(Resource.Id.cocktailImage);
            TextView cocktailName = FindViewById<TextView>(Resource.Id.cocktailName);
            TextView cocktailDescription = FindViewById<TextView>(Resource.Id.cocktailDescription);
            TextView cocktailProcedure = FindViewById<TextView>(Resource.Id.cocktailProcedure);

            LinearLayout ingredientList = FindViewById<LinearLayout>(Resource.Id.ingredientList);

            Cocktail cocktail = dbh.GetCocktail(cocktailId);

            cocktailName.Text = cocktail.Name;
            cocktailDescription.Text = cocktail.Description;
            cocktailProcedure.Text = cocktail.Procedure;

            foreach (var cocktailIngredient in dbh.GetCocktailIngredients(cocktail.ID))
            {
                Ingredient ingredient = dbh.GetIngredient(cocktailIngredient.IngredientId);
                TextView lblIngredient = new TextView(this) { Text = string.Format("{0} {1} {2}", cocktailIngredient.IngredientAmount, dbh.GetUnit(cocktailIngredient.UnitId).Name, ingredient.Name) };
                lblIngredient.SetTextAppearance(Resource.Style.TextAppearance_AppCompat_Small);
                ingredientList.AddView(lblIngredient);
            }

        }
    }
}