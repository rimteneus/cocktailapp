﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TheCocktailApp.Database;
using TheCocktailApp.Database.Tables;

namespace TheCocktailApp.Activities
{
    [Activity(Label = "CreateIngredientActivity", Theme = "@style/AppTheme")]
    public class CreateIngredientActivity : Activity
    {
        DatabaseHandler dbh;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.create_ingredient);
            // Create your application here
            dbh = new DatabaseHandler();

            Button btnSave = FindViewById<Button>(Resource.Id.btnSave);
            btnSave.Click += BtnSave_Click;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            EditText txtIngredientName = FindViewById<EditText>(Resource.Id.txtIngredientName);

            if(txtIngredientName.Text != "" || txtIngredientName.Text != null)
            {
                dbh.InsertIngredient(new Ingredient() { Name = txtIngredientName.Text });
                Finish();
            }
        }
    }
}