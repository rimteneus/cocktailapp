﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using TheCocktailApp.Database.Tables;

namespace TheCocktailApp.Database
{
    public class DatabaseHandler
    {
        private string path = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.ToString(), "cocktail.db");
        private static SQLiteConnection db;

        public DatabaseHandler()
        {
            CopyDatabaseIfNotExists(path);

            if (db == null)
            {
                db = new SQLiteConnection(path);
                CreateTables();
            }

            if (GetAllCocktails().Count == 0)
            {
                SetUpDatabase();
            }
            //AddSomeCocktails();
        }

        private static void CopyDatabaseIfNotExists(string dbPath)
        {
            if (!File.Exists(dbPath))
            {
                using (var br = new BinaryReader(Application.Context.Assets.Open("cocktail.db")))
                {
                    using (var bw = new BinaryWriter(new FileStream(dbPath, FileMode.Create)))
                    {
                        byte[] buffer = new byte[2048];
                        int length = 0;
                        while ((length = br.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            bw.Write(buffer, 0, length);
                        }
                    }
                }
            }
        }

        private void CreateTables()
        {
            db.CreateTable<Cocktail>();
            db.CreateTable<CocktailIngredient>();
            db.CreateTable<Ingredient>();
            db.CreateTable<Glass>();
            db.CreateTable<Unit>();
        }

        private void AddSomeCocktails()
        {
            if (GetAllCocktails().Count == 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    Cocktail cock = new Cocktail();
                    cock.Name = "Cocktail" + i;
                    cock.Description = "This cocktail is a delicious looking cocktail with a fresh taste and made with a lot of booze." + i;
                    cock.Procedure = "This drink is a large shot. Pour in 250ml in to a long drink glass and shot it m8." + i;
                    cock.GlassId = 1;
                    cock.AlcoholPercentage = 69;
                    db.Insert(cock);
                }
            }
        }

        public List<Cocktail> GetAllCocktails()
        {
            return db.Table<Cocktail>().ToList<Cocktail>();
        }

        public List<Ingredient> GetAllIngredients()
        {
            return db.Table<Ingredient>().ToList<Ingredient>();
        }

        public List<Glass> GetAllGlasses()
        {
            return db.Table<Glass>().ToList<Glass>();
        }

        public List<Unit> GetAllUnits()
        {
            return db.Table<Unit>().ToList<Unit>();
        }

        public Cocktail GetCocktail(int cocktailId)
        {
            return db.Get<Cocktail>(cocktailId);
        }

        public CocktailIngredient GetCocktailIngredient(int cocktailIngredientId)
        {
            return db.Get<CocktailIngredient>(cocktailIngredientId);
        }

        public Ingredient GetIngredient(int ingredientId)
        {
            return db.Get<Ingredient>(ingredientId);
        }

        public Glass GetGlass(int glassId)
        {
            return db.Get<Glass>(glassId);
        }

        public List<CocktailIngredient> GetCocktailIngredients(int cocktailId)
        {
            return db.Query<CocktailIngredient>("SELECT * FROM CocktailIngredient WHERE CocktailId = ?", cocktailId);
        }

        public bool InsertIngredient(Ingredient ingredient)
        {
            try
            {
                db.Insert(ingredient);
                return true;
            }
            catch (SQLite.SQLiteException)
            {
                return false;
            }
        }

        public bool InsertCocktail(Cocktail cocktail)
        {
            try
            {
                db.Insert(cocktail);
                return true;
            }
            catch (SQLite.SQLiteException)
            {
                return false;
            }
        }

        public bool InsertCocktailIngredient(CocktailIngredient cocktailIngredient)
        {
            try
            {
                db.Insert(cocktailIngredient);
                return true;
            }
            catch (SQLite.SQLiteException)
            {
                return false;
            }
        }

        public Unit GetUnit(int unitId)
        {
            return db.Get<Unit>(unitId);
        }

        public void InsertUnit(Unit unit)
        {
            db.Insert(unit);
        }

        public void InsertGlass(Glass glass)
        {
            db.Insert(glass);
        }

        /// <summary>
        /// Method for filling database for the first time.
        /// </summary>
        private void SetUpDatabase()
        {
            List<string> units = new List<string> { "shotje", "shots", "ml", "cl", "stuk", "stuks", "blaadje", "blaadjes" };

            foreach  (string unitName in units)
            {
                Unit unit = new Unit();
                unit.Name = unitName;
                InsertUnit(unit);
            }

            List<string> ingredients = new List<string> { "Witte Rum", "Bruine Rum", "Tequilla", "Vodka", "Whiskey", "Gin", "Licor43", "De Kuyper Watermeloen", "Spa Rood", "Limoensap", "Suikersiroop", "Blaadjes verse munt", "Ijsblokjes", "Citroensap", "Citroen", "Munt", "Sprite", "Cola", "Triple sec" };

            foreach (string ingredientName in ingredients)
            {
                Ingredient ingredient = new Ingredient();
                ingredient.Name = ingredientName;
                InsertIngredient(ingredient);
            }

            List<string> glasses = new List<string> { "Longdrink glas", "Martini glas", "Margaritaglas", "Shotglas", "Hurricane", "Tumbler", "Fluitje", "Tiki glas" };
            foreach (string glassName in glasses)
            {
                Glass glass = new Glass();
                glass.Name = glassName;
                InsertGlass(glass);
            }

            List<string> cocktailNames = new List<string>
            {
                "Watermelon Fizz",
                "Mojito",
                "Splitje",
                "Long island iced tea",
                "Blue lagoon"
            };

            List<int> cocktailPercentages = new List<int> { 20, 15, 10, 40, 30 };

            List<string> cocktailDescriptions = new List<string>
            {
                "De Watermelon Fizz een verfrissende cocktail die door De Kuyper Watermeloen likeur aan een sterke watermeloen smaak komt. Ideaal voor de zomer, of wanneer je zin hebt in een watermeloen!",
                "Mojito is een cocktail ideaal voor zomerdagen. Binnen 5 minuten zit jij lekker in het zonnetje met deze heerlijke cocktail!",
                "Hou je van een split ijsje? Dan is deze cocktail ideaal voor jou. Met liqor43, sinasappelsap en slagroom lijkt de smaak precies op die van een splitje!",
                "Met z’n hoge alcoholpercentage is deze cocktail net iets anders dan andere cocktails. De Long Island Iced Tea wordt gemaakt met een grote hoeveelheid verschillende sterke dranken en is daarmee legendarisch, maar tegelijkertijd ook berucht. Ontdek alles over deze cocktail en maak hem binnenkort zelf!",
                "Mooie zomerse cocktail met de helderheid van een tropische lagune en de smaak van een frisse bries! Deze blauwe zomercocktail is met name voor vrouwen een guilty pleasure. "
            };

            List<string> cocktailProcedures = new List<string>
            {
                "Pak een Londrink en zet deze voor je neer op de bar. Vul dit glas met ijsblokjes en schenk vervolgens 20ml Russian Standard Vodka en 20 ml De Kuyper Watermelon in het glas en gebruikt hierbij een maatje. Schenk nu 40 ml Finest Call Sweet & Sour in een maatje en schenk dit in het glas. Pak je barspoon en leg die boven het glas. Schenk vervolgens de Monin suiker siroop op de lepel totdat die gevuld is en laat het in het glas vallen. Top nu je cocktail af met Sprite en roer een aantal keer goed door het glas zodat alles goed met elkaar gemengd is. Doe er vervolgens nog een paar ijsblokjes bovenop zodat het glas er verzorgd en gevuld uitziet. Steek een lang zwart rietje in het glas en garneer het drankje af met een schijfje citroen en een takje munt.",
                "Schenk de rum, het versgeperste limoensap, het bruiswater en de suikersiroop in het Mojito glas Plaats de muntblaadjes tussen je handpalmen en klap (dit kan ook in het glas zelf gedaan worden met een stamper) Wrijf vervolgens met de blaadjes over de glasrand heen en laat ze daarna in het glas vallen Vul het glas tot net onder de rand met crushed ijs en roer om Garneer de Mojito met een munttakje en partjes limoen",
                "Pak een shot glaasje en vul deze voor 1/3 met Licor34. Vul vervolgens het glaasje vol met sinasappelsap en top het af met slagroom.",
                "De cocktail schenk je in een longdrink glas. Je kunt gebruik maken van een shaker. Als je geen shaker hebt moet je eerst het glas vullen met ijsblokjes. Doe vervolgens de Wodka, Rum, Tequila en de Gin erbij. Doe van iedere spirit niet meer dan een half barmaatje. Je kunt voor de smaak ook een halve citroen, citroensap en sap van de cocktailkersen toe voegen. Vervolgens kun je het glas opvullen met de cola. Doe voorzichtig een rietje in het glas. Als laatste kun je een schijfje citroen aan de rand prikken en doe er een cocktailkers bij. De cocktail is nu klaar om geserveerd te worden.",
                "Gebruik een longdrink glas of een tropisch hurricane glas. Je hebt geen shaker nodig want je maakt de cocktail in het glas. Vul het glas met ijs. Begin met het uitpersen van een halve citroen. Snijd ook alvast een mooi stuk af voor de garnering aan je glas. Giet daarna 1 barmaatje vodka en een half deel blue curacao. Vul hierna het glas met crushed ice en top af met 7up of sprite. Hierna even stirren / roeren in het glas met een lange barlepel. Garneer de Blue Lagoon als finishing touch met een schijfje citroen en een cocktailkers voor de kleur!"
            };

            for (int i = 0; i < 5; i++)
            {
                Cocktail cocktail = new Cocktail();
                cocktail.Name = cocktailNames[i];
                cocktail.Description = cocktailDescriptions[i];
                cocktail.Procedure = cocktailProcedures[i];
                cocktail.AlcoholPercentage = cocktailPercentages[i];
                cocktail.GlassId = 0;
                InsertCocktail(cocktail);
            }
        }

        private void CreateCocktailIngredients()
        {

        }

        private void CreateMojitoIngredients()
        {
            CocktailIngredient ingredient = new CocktailIngredient();
            ingredient.CocktailId = GetCocktail("Mojito").ID;
            ingredient.IngredientId = GetIngredient("Rum").ID;
            //ingredient.UnitId = GetU
        }

        public Ingredient GetIngredient(string ingredientName)
        {
            return db.FindWithQuery<Ingredient>(string.Format("SELECT * FROM Cocktail WHERE name={0}", ingredientName));
        }

        public Cocktail GetCocktail(string cocktailName)
        {
            return db.FindWithQuery<Cocktail>(string.Format("SELECT * FROM Cocktail WHERE name={0}", cocktailName));
        }
    }
}