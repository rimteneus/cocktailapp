﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace TheCocktailApp.Database.Tables
{
    [Table("Cocktail")]
    public class Cocktail
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int ID { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        public int AlcoholPercentage { get; set; }

        [MaxLength(250)]
        public string Description { get; set; }

        [MaxLength(500)]
        public string Procedure { get; set; }

        public int GlassId { get; set; }
        
    }
}