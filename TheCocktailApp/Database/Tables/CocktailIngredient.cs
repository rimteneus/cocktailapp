﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace TheCocktailApp.Database.Tables
{
    [Table("CocktailIngredient")]
    public class CocktailIngredient
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int ID { get; set; }

        public int CocktailId { get; set; }

        public int IngredientId { get; set; }

        public int IngredientAmount { get; set; }

        public int UnitId { get; set; }

    }
}