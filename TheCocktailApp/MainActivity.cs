﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using Android.Graphics.Drawables;
using System;
using System.Collections.Generic;
using SQLite;
using Android.Content;

namespace TheCocktailApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        
        Database.DatabaseHandler dbh;
        LinearLayout mainLayout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            dbh = new Database.DatabaseHandler();

            mainLayout = FindViewById<LinearLayout>(Resource.Id.mainLayout);

            SetupView();
        }

        private void SetupView()
        {
            mainLayout.RemoveAllViews();
            foreach (var cocktail in dbh.GetAllCocktails())
            {
                LinearLayout box = new LinearLayout(this) { LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FillParent, 250), Orientation = Orientation.Horizontal };
                LinearLayout details = new LinearLayout(this) { LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FillParent, ViewGroup.LayoutParams.FillParent), Orientation = Orientation.Vertical };
                LinearLayout cocktailName = new LinearLayout(this) { Orientation = Orientation.Horizontal };

                ImageView image = new ImageView(this) { LayoutParameters = new LinearLayout.LayoutParams(250, 250) };
                image.SetImageResource(Resource.Drawable.dank);

                TextView name = new TextView(this) { Text = cocktail.Name };
                name.SetTextAppearance(Resource.Style.TextAppearance_AppCompat_Large);

                TextView alcoholPercentage = new TextView(this) { Text = string.Format("{0}%", cocktail.AlcoholPercentage), Gravity = GravityFlags.CenterVertical, TextAlignment = TextAlignment.Center };
                alcoholPercentage.SetTextAppearance(Resource.Style.TextAppearance_AppCompat_Small);
                alcoholPercentage.SetPadding(10, 0, 0, 0);

                TextView description = new TextView(this) { Text = cocktail.Description };
                description.SetTextAppearance(Resource.Style.TextAppearance_AppCompat_Medium);

                cocktailName.AddView(name);
                cocktailName.AddView(alcoholPercentage);

                details.AddView(cocktailName);
                details.AddView(description);

                box.AddView(image);
                box.AddView(details);
                box.Id = cocktail.ID;
                box.Click += Box_Click;

                mainLayout.AddView(box);
            }

            Button btnAddCocktail = new Button(this) { Text = "Cocktail toevoegen" };
            btnAddCocktail.Click += btnAddCocktail_Click;
            mainLayout.AddView(btnAddCocktail);
        }

        private void btnAddCocktail_Click(object sender, EventArgs e)
        {
            Intent createActivity = new Intent(this, typeof(Activities.CreateActivity));
            StartActivity(createActivity);
        }

        private void Box_Click(object sender, EventArgs e)
        {
            LinearLayout layout = (LinearLayout)sender;
            Intent detailActiviy = new Intent(this, typeof(Activities.DetailActivity));
            detailActiviy.PutExtra("CocktailID", (int)layout.Id);
            StartActivity(detailActiviy);
        }


        override protected void OnResume()
        {
            base.OnResume();

            SetupView();
        }
    }
}